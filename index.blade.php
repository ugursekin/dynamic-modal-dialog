<!-- LARAVEL E ÖZEL BİR YAPI OLARAK AYARLANDI. EĞER PURE PHP OLARAK KULLANMAK İSTİYORSANIZ LARAVEL İ İLGİLENDİREN ETİKENTLERİ PHP HALİNE GETİREREK KULLANABİLİRSİNİZ. ÇALIŞIYOR SORUN YOK. BOOTSTRAP  -->

@foreach($sirketler as $sirket)
<tr class="odd gradeX">
    <td width="1%" class="fw-bold text-dark">{{$sirket->id}}</td>
    <td width="1%">
        <!-- <img src="../assets/img/user/user-1.jpg" class="rounded h-30px my-n1 mx-n1" /> -->
    </td>
    <td id="rowTitle{{$sirket->id}}">{{$sirket->firma_unvani}}</td>
    <td>
        <a href="{{route('dashboard.sirketler.show',$sirket->id)}}"><button type="button"
                class="btn btn-xs btn-primary me-1 mb-1">Detay</button></a>
        <!-- toggler -->
        <a href="#" id="{{$sirket->id}}" modal-url<?php echo
            $sirket->id;?>="{{route('dashboard.sirketler.destroy',$sirket->id)}}" class="btn btn-xs btn-danger me-1 mb-1
            delete-modal" data-bs-toggle="modal">Sil</a>


    </td>
</tr>
@endforeach

<!-- #modal-dialog -->
<div class="modal fade" id="modal-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Başlık</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="alert" id="modal-alert">
                    <h5><i class="fa fa-info-circle"></i> <span class="info"> Etiket</span></h5>
                    <p class="modal-text">Modal yazısı </p>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-white cancel" data-bs-dismiss="modal">Vazgeç</a>
                <form action="" method="" class="modal-form">
                    @csrf
                    <input type="submit" class="btn btn-white confirm" data-bs-dismiss="modal" aria-hidden="true"
                        value="Sil">
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #modal-dialog -->

<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM="
    crossorigin="anonymous"></script>
<!-- jquery -->

<!-- ================== Modal İçin ================== -->
<script>
    $(document).ready(function () {
        $(".delete-modal").click(function (event) {

            var id = event.target.id;
            var modalUrl = $(this).attr("modal-url" + id);

            $('#modal-dialog .info').text('[' + $('#rowTitle' + id).text() + '] SİLİNECEK');
            $('#modal-dialog .modal-title').text('Uyarı');
            $('#modal-dialog .modal-text').text('Bu işlem geri alınamaz. Silme işlemi yapmak istediğinizden emin misiniz?');
            $('#modal-dialog .cancel').text('Vazgeç');
            $('#modal-dialog .confirm').text('Sil');

            $('#modal-dialog .modal-form').attr('action', modalUrl);
            $('#modal-dialog .modal-form').attr('method', 'POST');
            $("#modal-dialog .modal-form").prepend('<input type="hidden" name="_method" value="DELETE">');

            $('#modal-alert').addClass('alert-danger');
            $('#modal-dialog').modal('show');
        });
    });
</script>
<!-- ================== Modal İçin ================== -->